# Demos of Flask Extensions

I've tried to keep each demo as isolated as possible in order to showcast 
that specific extension or group of extensions.

If you're interested in only one extension, switch to that extension's branch 
then install through **pip**, hopefully in a virtual environment, 
the requirements of that branch.

For example:

```
$ git checkout 00-hello_world
$ pip install -r requirements_common.txt
$ pip install -r requirements.txt
$ python app.py
```

If you're interested in running multiple examples, use the **requirements.txt** file
from the **master** branch, which contains all the requirements of all branches.

```
$ git checkout master
$ pip install -r requirements.txt
$ git checkout 00-hello_world
$ python app.py
```

# Extensions by Branches

### 00-hello_world

One of the simplest flask applications.

